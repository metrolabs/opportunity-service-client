/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.projects.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wgayton on 5/13/17.
 */
public class RetrofitOpportunityMeasureEconomics {
    final Logger LOG = LoggerFactory.getLogger(RetrofitOpportunityMeasureEconomics.class);
    private double[] costReductions;
    private double[] costReoccurring;
    private double totalCostReduction;
    private double totalReoccurringCost;

    private double[] presentValue;
    private double[] futureValue;

    private double netPresentValue;
    private double internalRateOfReturn;
    private double costReductionRatio;
    private double returnOnInvestment;
    private int usefulLifeYears;

    public RetrofitOpportunityMeasureEconomics() {}

    public double[] getCostReductions() {
        return costReductions;
    }

    public double getCostReductions(int i) {
        return costReductions[i];
    }

    public double[] getCostReoccurring() {
        return costReoccurring;
    }

    public double getCostReoccurring(int i) {
        return costReoccurring[i];
    }

    public double getTotalCostReduction() {
        return totalCostReduction;
    }

    public double getTotalReoccurringCost() {
        return totalReoccurringCost;
    }

    public double[] getPresentValue() {
        return presentValue;
    }

    public double getPresentValue(int i) {
        return presentValue[i];
    }

    public double[] getFutureValue() {
        return futureValue;
    }

    public double getFutureValue(int i) {
        return futureValue[i];
    }

    public double getNetPresentValue() {
        return netPresentValue;
    }

    public double getInternalRateOfReturn() {
        return internalRateOfReturn;
    }

    public double getCostReductionRatio() {
        return costReductionRatio;
    }

    public double getReturnOnInvestment() {
        return returnOnInvestment;
    }

    public int getUsefulLifeYears() {
        return usefulLifeYears;
    }
}
