/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated  
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RetrofitOpportunitySummaryAnnual {
    private String id;
    @JsonIgnore
    private String accountId;
    private String opportunityId;
    private DisaggregationSummaryType summaryType;
    private Double heating;
    private Double cooling;
    private Double internalLighting;
    private Double externalLighting;
    private Double ventilation;
    private Double pumps;
    private Double electricPlugLoads;
    private Double fossilFuelPlugLoads;
    private Double domesticHotWater;
    private Double solarHotWaterGeneration;
    private Double photovoltaicGeneration;
    private Double windTurbineGeneration;
    private Double internalHeatGain;
    private Double solarHeatGain;
    private Double interiorTemperature;
    private Double outsideTemperature;
    private Double floatingTemperature;
    private Double electricityConsumption;
    private Double fossilFuelConsumption;
    private Double hvacEnergyConsumption;
    private Double totalEnergyConsumption;
    private Double peakDemand;

    public RetrofitOpportunitySummaryAnnual() {}

    public String getId() {
        return id;
    }

    public RetrofitOpportunitySummaryAnnual setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public RetrofitOpportunitySummaryAnnual setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public RetrofitOpportunitySummaryAnnual setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
        return this;
    }

    public DisaggregationSummaryType getSummaryType() {
        return summaryType;
    }

    public RetrofitOpportunitySummaryAnnual setSummaryType(DisaggregationSummaryType summaryType) {
        this.summaryType = summaryType;
        return this;
    }

    public Double getHeating() {
        return heating;
    }

    public RetrofitOpportunitySummaryAnnual setHeating(Double heating) {
        this.heating = heating;
        return this;
    }

    public Double getCooling() {
        return cooling;
    }

    public RetrofitOpportunitySummaryAnnual setCooling(Double cooling) {
        this.cooling = cooling;
        return this;
    }

    public Double getInternalLighting() {
        return internalLighting;
    }

    public RetrofitOpportunitySummaryAnnual setInternalLighting(Double internalLighting) {
        this.internalLighting = internalLighting;
        return this;
    }

    public Double getExternalLighting() {
        return externalLighting;
    }

    public RetrofitOpportunitySummaryAnnual setExternalLighting(Double externalLighting) {
        this.externalLighting = externalLighting;
        return this;
    }

    public Double getVentilation() {
        return ventilation;
    }

    public RetrofitOpportunitySummaryAnnual setVentilation(Double ventilation) {
        this.ventilation = ventilation;
        return this;
    }

    public Double getPumps() {
        return pumps;
    }

    public RetrofitOpportunitySummaryAnnual setPumps(Double pumps) {
        this.pumps = pumps;
        return this;
    }

    public Double getElectricPlugLoads() {
        return electricPlugLoads;
    }

    public RetrofitOpportunitySummaryAnnual setElectricPlugLoads(Double electricPlugLoads) {
        this.electricPlugLoads = electricPlugLoads;
        return this;
    }

    public Double getFossilFuelPlugLoads() {
        return fossilFuelPlugLoads;
    }

    public RetrofitOpportunitySummaryAnnual setFossilFuelPlugLoads(Double fossilFuelPlugLoads) {
        this.fossilFuelPlugLoads = fossilFuelPlugLoads;
        return this;
    }

    public Double getDomesticHotWater() {
        return domesticHotWater;
    }

    public RetrofitOpportunitySummaryAnnual setDomesticHotWater(Double domesticHotWater) {
        this.domesticHotWater = domesticHotWater;
        return this;
    }

    public Double getSolarHotWaterGeneration() {
        return solarHotWaterGeneration;
    }

    public RetrofitOpportunitySummaryAnnual setSolarHotWaterGeneration(Double solarHotWaterGeneration) {
        this.solarHotWaterGeneration = solarHotWaterGeneration;
        return this;
    }

    public Double getPhotovoltaicGeneration() {
        return photovoltaicGeneration;
    }

    public RetrofitOpportunitySummaryAnnual setPhotovoltaicGeneration(Double photovoltaicGeneration) {
        this.photovoltaicGeneration = photovoltaicGeneration;
        return this;
    }

    public Double getWindTurbineGeneration() {
        return windTurbineGeneration;
    }

    public RetrofitOpportunitySummaryAnnual setWindTurbineGeneration(Double windTurbineGeneration) {
        this.windTurbineGeneration = windTurbineGeneration;
        return this;
    }

    public Double getInternalHeatGain() {
        return internalHeatGain;
    }

    public RetrofitOpportunitySummaryAnnual setInternalHeatGain(Double internalHeatGain) {
        this.internalHeatGain = internalHeatGain;
        return this;
    }

    public Double getSolarHeatGain() {
        return solarHeatGain;
    }

    public RetrofitOpportunitySummaryAnnual setSolarHeatGain(Double solarHeatGain) {
        this.solarHeatGain = solarHeatGain;
        return this;
    }

    public Double getInteriorTemperature() {
        return interiorTemperature;
    }

    public RetrofitOpportunitySummaryAnnual setInteriorTemperature(Double interiorTemperature) {
        this.interiorTemperature = interiorTemperature;
        return this;
    }

    public Double getOutsideTemperature() {
        return outsideTemperature;
    }

    public RetrofitOpportunitySummaryAnnual setOutsideTemperature(Double outsideTemperature) {
        this.outsideTemperature = outsideTemperature;
        return this;
    }

    public Double getFloatingTemperature() {
        return floatingTemperature;
    }

    public RetrofitOpportunitySummaryAnnual setFloatingTemperature(Double floatingTemperature) {
        this.floatingTemperature = floatingTemperature;
        return this;
    }

    public Double getElectricityConsumption() {
        return electricityConsumption;
    }

    public RetrofitOpportunitySummaryAnnual setElectricityConsumption(Double electricityConsumption) {
        this.electricityConsumption = electricityConsumption;
        return this;
    }

    public Double getFossilFuelConsumption() {
        return fossilFuelConsumption;
    }

    public RetrofitOpportunitySummaryAnnual setFossilFuelConsumption(Double fossilFuelConsumption) {
        this.fossilFuelConsumption = fossilFuelConsumption;
        return this;
    }

    public Double getHvacEnergyConsumption() {
        return hvacEnergyConsumption;
    }

    public RetrofitOpportunitySummaryAnnual setHvacEnergyConsumption(Double hvacEnergyConsumption) {
        this.hvacEnergyConsumption = hvacEnergyConsumption;
        return this;
    }

    public Double getTotalEnergyConsumption() {
        return totalEnergyConsumption;
    }

    public RetrofitOpportunitySummaryAnnual setTotalEnergyConsumption(Double totalEnergyConsumption) {
        this.totalEnergyConsumption = totalEnergyConsumption;
        return this;
    }

    public Double getPeakDemand() {
        return peakDemand;
    }

    public RetrofitOpportunitySummaryAnnual setPeakDemand(Double peakDemand) {
        this.peakDemand = peakDemand;
        return this;
    }
}
