/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated  
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.metro.buildings.models.buildings.Building;
import org.springframework.hateoas.Resource;

import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RetrofitOpportunity {
    private String id;
    private String accountId;
    private String buildingId;
    private String baselineId;
    private String description;
    private Integer usefulLifeYears;
    private Double reoccurringCost;
    private Double discountRate;
    private Double intRateOfReturn;
    private Double netPresentValue;
    private Double returnOnInvestment;
    private Double valueAtRisk;
    private Double performanceRisk;
    private String simulationId;
    private String name;
    private Double initialCost;
    private Double grossCost;
    private Double netCost;
    private Double incentive;
    private Double expectedCostReduction;
    private Double expectedEnergyReduction;
    private Double expectedDemandReduction;
    private Double expectedEmissionsReduction;
    private Integer expectedPaybackYears;
    private Double electricPriceKwh;
    private Double electricCo2Kwh;
    private Double fossilFuelPriceKwh;
    private Double fossilFuelCo2Kwh;
    private Double projectedCostReduction;
    private Double projectedCostReductionVariation;
    private Double projectedEnergyReduction;
    private Double projectedEnergyReductionVariation;
    private Double projectedDemandReduction;
    private Double projectedDemandReductionVariation;
    private Double projectedEmissionsReduction;
    private Double projectedEmissionsReductionVariation;
    private Integer projectedPaybackYears;
    private Double projectedPaybackYearsVariation;
    private Double totalIncentive;
    private String riskRating;

    private Double costReductionRatio;
    private Double energyReductionIntensity;
    private Double costReductionIntensity;
    private Double emissionsReductionIntensity;

    private Map<String, RetrofitOpportunityDetail> details;
    private List<RetrofitOpportunityMeasure> measures;
    private Map<DisaggregationSummaryType, RetrofitOpportunitySummaryAnnual> annualSummary;
    private Map<DisaggregationSummaryType,List<RetrofitOpportunitySummaryMonthly>> monthlySummary;
    private Resource<Building> building;

    public RetrofitOpportunity() {}

    public String getId() {
        return id;
    }

    public RetrofitOpportunity setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public RetrofitOpportunity setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public RetrofitOpportunity setBuildingId(String buildingId) {
        this.buildingId = buildingId;
        return this;
    }

    public String getBaselineId() {
        return baselineId;
    }

    public RetrofitOpportunity setBaselineId(String baselineId) {
        this.baselineId = baselineId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public RetrofitOpportunity setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getUsefulLifeYears() {
        return usefulLifeYears;
    }

    public RetrofitOpportunity setUsefulLifeYears(Integer usefulLifeYears) {
        this.usefulLifeYears = usefulLifeYears;
        return this;
    }

    public Double getReoccurringCost() {
        return reoccurringCost;
    }

    public RetrofitOpportunity setReoccurringCost(Double reoccurringCost) {
        this.reoccurringCost = reoccurringCost;
        return this;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public RetrofitOpportunity setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
        return this;
    }

    public Double getCostReductionRatio() {
        return costReductionRatio;
    }

    public RetrofitOpportunity setCostReductionRatio(Double costReductionRatio) {
        this.costReductionRatio = costReductionRatio;
        return this;
    }

    public Double getIntRateOfReturn() {
        return intRateOfReturn;
    }

    public RetrofitOpportunity setIntRateOfReturn(Double intRateOfReturn) {
        this.intRateOfReturn = intRateOfReturn;
        return this;
    }

    public Double getNetPresentValue() {
        return netPresentValue;
    }

    public RetrofitOpportunity setNetPresentValue(Double netPresentValue) {
        this.netPresentValue = netPresentValue;
        return this;
    }

    public Double getReturnOnInvestment() {
        return returnOnInvestment;
    }

    public RetrofitOpportunity setReturnOnInvestment(Double returnOnInvestment) {
        this.returnOnInvestment = returnOnInvestment;
        return this;
    }

    public Double getValueAtRisk() {
        return valueAtRisk;
    }

    public RetrofitOpportunity setValueAtRisk(Double valueAtRisk) {
        this.valueAtRisk = valueAtRisk;
        return this;
    }

    public Double getPerformanceRisk() {
        return performanceRisk;
    }

    public RetrofitOpportunity setPerformanceRisk(Double performanceRisk) {
        this.performanceRisk = performanceRisk;
        return this;
    }

    public String getSimulationId() {
        return simulationId;
    }

    public RetrofitOpportunity setSimulationId(String simulationId) {
        this.simulationId = simulationId;
        return this;
    }

    public String getName() {
        return name;
    }

    public RetrofitOpportunity setName(String name) {
        this.name = name;
        return this;
    }

    public Double getInitialCost() {
        return initialCost;
    }

    public RetrofitOpportunity setInitialCost(Double initialCost) {
        this.initialCost = initialCost;
        return this;
    }

    public Double getGrossCost() {
        return grossCost;
    }

    public RetrofitOpportunity setGrossCost(Double grossCost) {
        this.grossCost = grossCost;
        return this;
    }

    public Double getNetCost() {
        return netCost;
    }

    public RetrofitOpportunity setNetCost(Double netCost) {
        this.netCost = netCost;
        return this;
    }

    public Double getIncentive() {
        return incentive;
    }

    public RetrofitOpportunity setIncentive(Double incentive) {
        this.incentive = incentive;
        return this;
    }

    public Double getExpectedCostReduction() {
        return expectedCostReduction;
    }

    public RetrofitOpportunity setExpectedCostReduction(Double expectedCostReduction) {
        this.expectedCostReduction = expectedCostReduction;
        return this;
    }

    public Double getExpectedEnergyReduction() {
        return expectedEnergyReduction;
    }

    public RetrofitOpportunity setExpectedEnergyReduction(Double expectedEnergyReduction) {
        this.expectedEnergyReduction = expectedEnergyReduction;
        return this;
    }

    public Double getExpectedDemandReduction() {
        return expectedDemandReduction;
    }

    public RetrofitOpportunity setExpectedDemandReduction(Double expectedDemandReduction) {
        this.expectedDemandReduction = expectedDemandReduction;
        return this;
    }

    public Double getExpectedEmissionsReduction() {
        return expectedEmissionsReduction;
    }

    public RetrofitOpportunity setExpectedEmissionsReduction(Double expectedEmissionsReduction) {
        this.expectedEmissionsReduction = expectedEmissionsReduction;
        return this;
    }

    public Integer getExpectedPaybackYears() {
        return expectedPaybackYears;
    }

    public RetrofitOpportunity setExpectedPaybackYears(Integer expectedPaybackYears) {
        this.expectedPaybackYears = expectedPaybackYears;
        return this;
    }

    public Double getElectricPriceKwh() {
        return electricPriceKwh;
    }

    public RetrofitOpportunity setElectricPriceKwh(Double electricPriceKwh) {
        this.electricPriceKwh = electricPriceKwh;
        return this;
    }

    public Double getElectricCo2Kwh() {
        return electricCo2Kwh;
    }

    public RetrofitOpportunity setElectricCo2Kwh(Double electricCo2Kwh) {
        this.electricCo2Kwh = electricCo2Kwh;
        return this;
    }

    public Double getFossilFuelPriceKwh() {
        return fossilFuelPriceKwh;
    }

    public RetrofitOpportunity setFossilFuelPriceKwh(Double fossilFuelPriceKwh) {
        this.fossilFuelPriceKwh = fossilFuelPriceKwh;
        return this;
    }

    public Double getFossilFuelCo2Kwh() {
        return fossilFuelCo2Kwh;
    }

    public RetrofitOpportunity setFossilFuelCo2Kwh(Double fossilFuelCo2Kwh) {
        this.fossilFuelCo2Kwh = fossilFuelCo2Kwh;
        return this;
    }

    public Double getProjectedCostReduction() {
        return projectedCostReduction;
    }

    public RetrofitOpportunity setProjectedCostReduction(Double projectedCostReduction) {
        this.projectedCostReduction = projectedCostReduction;
        return this;
    }

    public Double getProjectedCostReductionVariation() {
        return projectedCostReductionVariation;
    }

    public RetrofitOpportunity setProjectedCostReductionVariation(Double projectedCostReductionVariation) {
        this.projectedCostReductionVariation = projectedCostReductionVariation;
        return this;
    }

    public Double getProjectedEnergyReduction() {
        return projectedEnergyReduction;
    }

    public RetrofitOpportunity setProjectedEnergyReduction(Double projectedEnergyReduction) {
        this.projectedEnergyReduction = projectedEnergyReduction;
        return this;
    }

    public Double getProjectedEnergyReductionVariation() {
        return projectedEnergyReductionVariation;
    }

    public RetrofitOpportunity setProjectedEnergyReductionVariation(Double projectedEnergyReductionVariation) {
        this.projectedEnergyReductionVariation = projectedEnergyReductionVariation;
        return this;
    }

    public Double getProjectedDemandReduction() {
        return projectedDemandReduction;
    }

    public RetrofitOpportunity setProjectedDemandReduction(Double projectedDemandReduction) {
        this.projectedDemandReduction = projectedDemandReduction;
        return this;
    }

    public Double getProjectedDemandReductionVariation() {
        return projectedDemandReductionVariation;
    }

    public RetrofitOpportunity setProjectedDemandReductionVariation(Double projectedDemandReductionVariation) {
        this.projectedDemandReductionVariation = projectedDemandReductionVariation;
        return this;
    }

    public Double getProjectedEmissionsReduction() {
        return projectedEmissionsReduction;
    }

    public RetrofitOpportunity setProjectedEmissionsReduction(Double projectedEmissionsReduction) {
        this.projectedEmissionsReduction = projectedEmissionsReduction;
        return this;
    }

    public Double getProjectedEmissionsReductionVariation() {
        return projectedEmissionsReductionVariation;
    }

    public RetrofitOpportunity setProjectedEmissionsReductionVariation(Double projectedEmissionsReductionVariation) {
        this.projectedEmissionsReductionVariation = projectedEmissionsReductionVariation;
        return this;
    }

    public Double getProjectedPaybackYearsVariation() {
        return projectedPaybackYearsVariation;
    }

    public RetrofitOpportunity setProjectedPaybackYearsVariation(Double projectedPaybackYearsVariation) {
        this.projectedPaybackYearsVariation = projectedPaybackYearsVariation;
        return this;
    }

    public Integer getProjectedPaybackYears() {
        return projectedPaybackYears;
    }

    public RetrofitOpportunity setProjectedPaybackYears(Integer projectedPaybackYears) {
        this.projectedPaybackYears = projectedPaybackYears;
        return this;
    }

    public Double getTotalIncentive() {
        return totalIncentive;
    }

    public RetrofitOpportunity setTotalIncentive(Double totalIncentive) {
        this.totalIncentive = totalIncentive;
        return this;
    }

    public String getRiskRating() {
        return riskRating;
    }

    public RetrofitOpportunity setRiskRating(String riskRating) {
        this.riskRating = riskRating;
        return this;
    }

    public Double getEnergyReductionIntensity() {
        return energyReductionIntensity;
    }

    public RetrofitOpportunity setEnergyReductionIntensity(Double energyReductionIntensity) {
        this.energyReductionIntensity = energyReductionIntensity;
        return this;
    }

    public Double getCostReductionIntensity() {
        return costReductionIntensity;
    }

    public RetrofitOpportunity setCostReductionIntensity(Double costReductionIntensity) {
        this.costReductionIntensity = costReductionIntensity;
        return this;
    }

    public Double getEmissionsReductionIntensity() {
        return emissionsReductionIntensity;
    }

    public RetrofitOpportunity setEmissionsReductionIntensity(Double emissionsReductionIntensity) {
        this.emissionsReductionIntensity = emissionsReductionIntensity;
        return this;
    }

    public Map<String, RetrofitOpportunityDetail> getDetails() {
        return details;
    }

    public RetrofitOpportunity setDetails(Map<String, RetrofitOpportunityDetail> details) {
        this.details = details;
        return this;
    }

    public List<RetrofitOpportunityMeasure> getMeasures() {
        return measures;
    }

    public RetrofitOpportunity setMeasures(List<RetrofitOpportunityMeasure> measures) {
        this.measures = measures;
        return this;
    }

    public Map<DisaggregationSummaryType, RetrofitOpportunitySummaryAnnual> getAnnualSummary() {
        return annualSummary;
    }

    public RetrofitOpportunity setAnnualSummary(Map<DisaggregationSummaryType, RetrofitOpportunitySummaryAnnual> annualSummary) {
        this.annualSummary = annualSummary;
        return this;
    }

    public Map<DisaggregationSummaryType,List<RetrofitOpportunitySummaryMonthly>> getMonthlySummary() {
        return monthlySummary;
    }

    public RetrofitOpportunity setMonthlySummary(Map<DisaggregationSummaryType,List<RetrofitOpportunitySummaryMonthly>> monthlySummary) {
        this.monthlySummary = monthlySummary;
        return this;
    }

    public Resource<Building> getBuilding() {
        return building;
    }

    public RetrofitOpportunity setBuilding(Resource<Building> building) {
        this.building = building;
        return this;
    }
}
