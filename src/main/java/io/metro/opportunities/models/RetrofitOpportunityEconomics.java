/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.metro.projects.models.RetrofitOpportunityMeasureEconomics;

import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RetrofitOpportunityEconomics {

    private List<Double> costReductions;
    private List<Double> costReoccurring;

    private List<Double> presentValue;
    private List<Double> futureValue;

    private double netPresentValue;
    private double internalRateOfReturn;
    private double costReductionRatio;
    private double returnOnInvestment;

    private Map<String, RetrofitOpportunityMeasureEconomics> measures;

    public RetrofitOpportunityEconomics() {}

    public List<Double> getCostReductions() {
        return costReductions;
    }

    public RetrofitOpportunityEconomics setCostReductions(List<Double> costReductions) {
        this.costReductions = costReductions;
        return this;
    }

    public List<Double> getCostReoccurring() {
        return costReoccurring;
    }

    public RetrofitOpportunityEconomics setCostReoccurring(List<Double> costReoccurring) {
        this.costReoccurring = costReoccurring;
        return this;
    }

    public List<Double> getPresentValue() {
        return presentValue;
    }

    public RetrofitOpportunityEconomics setPresentValue(List<Double> presentValue) {
        this.presentValue = presentValue;
        return this;
    }

    public List<Double> getFutureValue() {
        return futureValue;
    }

    public RetrofitOpportunityEconomics setFutureValue(List<Double> futureValue) {
        this.futureValue = futureValue;
        return this;
    }

    public double getNetPresentValue() {
        return netPresentValue;
    }

    public RetrofitOpportunityEconomics setNetPresentValue(double netPresentValue) {
        this.netPresentValue = netPresentValue;
        return this;
    }

    public double getInternalRateOfReturn() {
        return internalRateOfReturn;
    }

    public RetrofitOpportunityEconomics setInternalRateOfReturn(double internalRateOfReturn) {
        this.internalRateOfReturn = internalRateOfReturn;
        return this;
    }

    public double getCostReductionRatio() {
        return costReductionRatio;
    }

    public RetrofitOpportunityEconomics setCostReductionRatio(double costReductionRatio) {
        this.costReductionRatio = costReductionRatio;
        return this;
    }

    public double getReturnOnInvestment() {
        return returnOnInvestment;
    }

    public RetrofitOpportunityEconomics setReturnOnInvestment(double returnOnInvestment) {
        this.returnOnInvestment = returnOnInvestment;
        return this;
    }

    public Map<String, RetrofitOpportunityMeasureEconomics> getMeasures() {
        return measures;
    }

    public RetrofitOpportunityEconomics setMeasures(Map<String, RetrofitOpportunityMeasureEconomics> measures) {
        this.measures = measures;
        return this;
    }
}
