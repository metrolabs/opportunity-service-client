/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RetrofitOpportunityDetail {
    private String id;
    @JsonIgnore
    private String accountId;
    private String opportunityId;
    private String detailKey;
    private String unitOfMeasure;
    private Double baselineReferenceValue;
    private Double scenarioReferenceValue;
    private Double scenarioMinimumValue;
    private Double scenarioAverageValue;
    private Double scenarioMaximumValue;
    private Double minimumReduction;
    private Double averageReduction;
    private Double maximumReduction;
    private Double scenarioVariance;
    private Double scenarioVariancePercentage;

    public RetrofitOpportunityDetail() {}

    public String getId() {
        return id;
    }

    public RetrofitOpportunityDetail setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public RetrofitOpportunityDetail setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public RetrofitOpportunityDetail setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
        return this;
    }

    public String getDetailKey() {
        return detailKey;
    }

    public RetrofitOpportunityDetail setDetailKey(String detailKey) {
        this.detailKey = detailKey;
        return this;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public RetrofitOpportunityDetail setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
        return this;
    }

    public Double getBaselineReferenceValue() {
        return baselineReferenceValue;
    }

    public RetrofitOpportunityDetail setBaselineReferenceValue(Double baselineReferenceValue) {
        this.baselineReferenceValue = baselineReferenceValue;
        return this;
    }

    public Double getScenarioReferenceValue() {
        return scenarioReferenceValue;
    }

    public RetrofitOpportunityDetail setScenarioReferenceValue(Double scenarioReferenceValue) {
        this.scenarioReferenceValue = scenarioReferenceValue;
        return this;
    }

    public Double getScenarioMinimumValue() {
        return scenarioMinimumValue;
    }

    public RetrofitOpportunityDetail setScenarioMinimumValue(Double scenarioMinimumValue) {
        this.scenarioMinimumValue = scenarioMinimumValue;
        return this;
    }

    public Double getScenarioAverageValue() {
        return scenarioAverageValue;
    }

    public RetrofitOpportunityDetail setScenarioAverageValue(Double scenarioAverageValue) {
        this.scenarioAverageValue = scenarioAverageValue;
        return this;
    }

    public Double getScenarioMaximumValue() {
        return scenarioMaximumValue;
    }

    public RetrofitOpportunityDetail setScenarioMaximumValue(Double scenarioMaximumValue) {
        this.scenarioMaximumValue = scenarioMaximumValue;
        return this;
    }

    public Double getMinimumReduction() {
        return minimumReduction;
    }

    public RetrofitOpportunityDetail setMinimumReduction(Double minimumReduction) {
        this.minimumReduction = minimumReduction;
        return this;
    }

    public Double getAverageReduction() {
        return averageReduction;
    }

    public RetrofitOpportunityDetail setAverageReduction(Double averageReduction) {
        this.averageReduction = averageReduction;
        return this;
    }

    public Double getMaximumReduction() {
        return maximumReduction;
    }

    public RetrofitOpportunityDetail setMaximumReduction(Double maximumReduction) {
        this.maximumReduction = maximumReduction;
        return this;
    }

    public Double getScenarioVariance() {
        return scenarioVariance;
    }

    public RetrofitOpportunityDetail setScenarioVariance(Double scenarioVariance) {
        this.scenarioVariance = scenarioVariance;
        return this;
    }

    public Double getScenarioVariancePercentage() {
        return scenarioVariancePercentage;
    }

    public RetrofitOpportunityDetail setScenarioVariancePercentage(Double scenarioVariancePercentage) {
        this.scenarioVariancePercentage = scenarioVariancePercentage;
        return this;
    }
}
