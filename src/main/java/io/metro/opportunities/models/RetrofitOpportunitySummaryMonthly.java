/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated  
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RetrofitOpportunitySummaryMonthly {
    private String id;
    @JsonIgnore
    private String accountId;
    private String opportunityId;
    private DisaggregationSummaryType summaryType;
    private String monthName;
    private Integer monthIndex;
    private Double heating;
    private Double cooling;
    private Double internalLighting;
    private Double externalLighting;
    private Double ventilation;
    private Double pumps;
    private Double electricPlugLoads;
    private Double fossilFuelPlugLoads;
    private Double domesticHotWater;
    private Double solarHotWaterGeneration;
    private Double photovoltaicGeneration;
    private Double windTurbineGeneration;
    private Double internalHeatGain;
    private Double solarHeatGain;
    private Double interiorTemperature;
    private Double outsideTemperature;
    private Double floatingTemperature;
    private Double electricityConsumption;
    private Double fossilFuelConsumption;
    private Double hvacEnergyConsumption;
    private Double totalEnergyConsumption;
    private Integer peakDemandDay;
    private Integer peakDemandHour;
    private Double peakDemand;

    public RetrofitOpportunitySummaryMonthly() {}

    public String getId() {
        return id;
    }

    public RetrofitOpportunitySummaryMonthly setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public RetrofitOpportunitySummaryMonthly setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public RetrofitOpportunitySummaryMonthly setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
        return this;
    }

    public DisaggregationSummaryType getSummaryType() {
        return summaryType;
    }

    public RetrofitOpportunitySummaryMonthly setSummaryType(DisaggregationSummaryType summaryType) {
        this.summaryType = summaryType;
        return this;
    }

    public String getMonthName() {
        return monthName;
    }

    public RetrofitOpportunitySummaryMonthly setMonthName(String monthName) {
        this.monthName = monthName;
        return this;
    }

    public Integer getMonthIndex() {
        return monthIndex;
    }

    public RetrofitOpportunitySummaryMonthly setMonthIndex(Integer monthIndex) {
        this.monthIndex = monthIndex;
        return this;
    }

    public Double getHeating() {
        return heating;
    }

    public RetrofitOpportunitySummaryMonthly setHeating(Double heating) {
        this.heating = heating;
        return this;
    }

    public Double getCooling() {
        return cooling;
    }

    public RetrofitOpportunitySummaryMonthly setCooling(Double cooling) {
        this.cooling = cooling;
        return this;
    }

    public Double getInternalLighting() {
        return internalLighting;
    }

    public RetrofitOpportunitySummaryMonthly setInternalLighting(Double internalLighting) {
        this.internalLighting = internalLighting;
        return this;
    }

    public Double getExternalLighting() {
        return externalLighting;
    }

    public RetrofitOpportunitySummaryMonthly setExternalLighting(Double externalLighting) {
        this.externalLighting = externalLighting;
        return this;
    }

    public Double getVentilation() {
        return ventilation;
    }

    public RetrofitOpportunitySummaryMonthly setVentilation(Double ventilation) {
        this.ventilation = ventilation;
        return this;
    }

    public Double getPumps() {
        return pumps;
    }

    public RetrofitOpportunitySummaryMonthly setPumps(Double pumps) {
        this.pumps = pumps;
        return this;
    }

    public Double getElectricPlugLoads() {
        return electricPlugLoads;
    }

    public RetrofitOpportunitySummaryMonthly setElectricPlugLoads(Double electricPlugLoads) {
        this.electricPlugLoads = electricPlugLoads;
        return this;
    }

    public Double getFossilFuelPlugLoads() {
        return fossilFuelPlugLoads;
    }

    public RetrofitOpportunitySummaryMonthly setFossilFuelPlugLoads(Double fossilFuelPlugLoads) {
        this.fossilFuelPlugLoads = fossilFuelPlugLoads;
        return this;
    }

    public Double getDomesticHotWater() {
        return domesticHotWater;
    }

    public RetrofitOpportunitySummaryMonthly setDomesticHotWater(Double domesticHotWater) {
        this.domesticHotWater = domesticHotWater;
        return this;
    }

    public Double getSolarHotWaterGeneration() {
        return solarHotWaterGeneration;
    }

    public RetrofitOpportunitySummaryMonthly setSolarHotWaterGeneration(Double solarHotWaterGeneration) {
        this.solarHotWaterGeneration = solarHotWaterGeneration;
        return this;
    }

    public Double getPhotovoltaicGeneration() {
        return photovoltaicGeneration;
    }

    public RetrofitOpportunitySummaryMonthly setPhotovoltaicGeneration(Double photovoltaicGeneration) {
        this.photovoltaicGeneration = photovoltaicGeneration;
        return this;
    }

    public Double getWindTurbineGeneration() {
        return windTurbineGeneration;
    }

    public RetrofitOpportunitySummaryMonthly setWindTurbineGeneration(Double windTurbineGeneration) {
        this.windTurbineGeneration = windTurbineGeneration;
        return this;
    }

    public Double getInternalHeatGain() {
        return internalHeatGain;
    }

    public RetrofitOpportunitySummaryMonthly setInternalHeatGain(Double internalHeatGain) {
        this.internalHeatGain = internalHeatGain;
        return this;
    }

    public Double getSolarHeatGain() {
        return solarHeatGain;
    }

    public RetrofitOpportunitySummaryMonthly setSolarHeatGain(Double solarHeatGain) {
        this.solarHeatGain = solarHeatGain;
        return this;
    }

    public Double getInteriorTemperature() {
        return interiorTemperature;
    }

    public RetrofitOpportunitySummaryMonthly setInteriorTemperature(Double interiorTemperature) {
        this.interiorTemperature = interiorTemperature;
        return this;
    }

    public Double getOutsideTemperature() {
        return outsideTemperature;
    }

    public RetrofitOpportunitySummaryMonthly setOutsideTemperature(Double outsideTemperature) {
        this.outsideTemperature = outsideTemperature;
        return this;
    }

    public Double getFloatingTemperature() {
        return floatingTemperature;
    }

    public RetrofitOpportunitySummaryMonthly setFloatingTemperature(Double floatingTemperature) {
        this.floatingTemperature = floatingTemperature;
        return this;
    }

    public Double getElectricityConsumption() {
        return electricityConsumption;
    }

    public RetrofitOpportunitySummaryMonthly setElectricityConsumption(Double electricityConsumption) {
        this.electricityConsumption = electricityConsumption;
        return this;
    }

    public Double getFossilFuelConsumption() {
        return fossilFuelConsumption;
    }

    public RetrofitOpportunitySummaryMonthly setFossilFuelConsumption(Double fossilFuelConsumption) {
        this.fossilFuelConsumption = fossilFuelConsumption;
        return this;
    }

    public Double getHvacEnergyConsumption() {
        return hvacEnergyConsumption;
    }

    public RetrofitOpportunitySummaryMonthly setHvacEnergyConsumption(Double hvacEnergyConsumption) {
        this.hvacEnergyConsumption = hvacEnergyConsumption;
        return this;
    }

    public Double getTotalEnergyConsumption() {
        return totalEnergyConsumption;
    }

    public RetrofitOpportunitySummaryMonthly setTotalEnergyConsumption(Double totalEnergyConsumption) {
        this.totalEnergyConsumption = totalEnergyConsumption;
        return this;
    }

    public Integer getPeakDemandDay() {
        return peakDemandDay;
    }

    public RetrofitOpportunitySummaryMonthly setPeakDemandDay(Integer peakDemandDay) {
        this.peakDemandDay = peakDemandDay;
        return this;
    }

    public Integer getPeakDemandHour() {
        return peakDemandHour;
    }

    public RetrofitOpportunitySummaryMonthly setPeakDemandHour(Integer peakDemandHour) {
        this.peakDemandHour = peakDemandHour;
        return this;
    }

    public Double getPeakDemand() {
        return peakDemand;
    }

    public RetrofitOpportunitySummaryMonthly setPeakDemand(Double peakDemand) {
        this.peakDemand = peakDemand;
        return this;
    }
}
