/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

/**
 * Created by wgayton on 5/17/17.
 */
public enum DisaggregationSummaryType {
    SCENARIO_LOWER_HINGE,
    SCENARIO_MIDDLE_HINGE,
    SCENARIO_UPPER_HINGE,
    SCENARIO_MIDDLE_SAVINGS,
    BASELINE_LOWER_HINGE,
    BASELINE_MIDDLE_HINGE,
    BASELINE_UPPER_HINGE
}
