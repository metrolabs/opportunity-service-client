/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RetrofitOpportunityMeasureInput {
    private String id;
    @JsonIgnore
    private String accountId;
    private String assetId;
    private String measureId;
    private String inputId;
    private String priorValue;
    private String retrofitValue;
    private String modelSystem;
    private String modelComponent;
    private String inputKey;

    public RetrofitOpportunityMeasureInput() {}

    public String getId() {
        return id;
    }

    public RetrofitOpportunityMeasureInput setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public RetrofitOpportunityMeasureInput setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getAssetId() {
        return assetId;
    }

    public RetrofitOpportunityMeasureInput setAssetId(String assetId) {
        this.assetId = assetId;
        return this;
    }

    public String getMeasureId() {
        return measureId;
    }

    public RetrofitOpportunityMeasureInput setMeasureId(String measureId) {
        this.measureId = measureId;
        return this;
    }

    public String getInputId() {
        return inputId;
    }

    public RetrofitOpportunityMeasureInput setInputId(String inputId) {
        this.inputId = inputId;
        return this;
    }

    public String getPriorValue() {
        return priorValue;
    }

    public RetrofitOpportunityMeasureInput setPriorValue(String priorValue) {
        this.priorValue = priorValue;
        return this;
    }

    public String getRetrofitValue() {
        return retrofitValue;
    }

    public RetrofitOpportunityMeasureInput setRetrofitValue(String retrofitValue) {
        this.retrofitValue = retrofitValue;
        return this;
    }

    public String getModelSystem() {
        return modelSystem;
    }

    public RetrofitOpportunityMeasureInput setModelSystem(String modelSystem) {
        this.modelSystem = modelSystem;
        return this;
    }

    public String getModelComponent() {
        return modelComponent;
    }

    public RetrofitOpportunityMeasureInput setModelComponent(String modelComponent) {
        this.modelComponent = modelComponent;
        return this;
    }

    public String getInputKey() {
        return inputKey;
    }

    public RetrofitOpportunityMeasureInput setInputKey(String inputKey) {
        this.inputKey = inputKey;
        return this;
    }
}
