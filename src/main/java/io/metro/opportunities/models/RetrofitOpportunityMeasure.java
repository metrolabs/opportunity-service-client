/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RetrofitOpportunityMeasure {
    private String id;
    @JsonIgnore
    private String accountId;
    private String opportunityId;
    private String assetId;
    private String systemId;
    private String componentId;
    private String name;
    private String description;
    private Integer expectedPaybackYears;
    private Integer usefulLifeYears;
    private Double reoccurringCost;
    private Double discountRate;
    private Double incentive;
    private Double intRateOfReturn;
    private Double netPresentValue;
    private Double returnOnInvestment;
    private Double valueAtRisk;
    private Double performanceRisk;
    private Double initialCost;
    private Double grossCost;
    private Double netCost;
    private Double expectedCostReduction;
    private Double expectedEnergyReduction;
    private Double expectedDemandReduction;
    private Double expectedEmissionsReduction;
    private Double projectedCostReduction;
    private Double projectedCostReductionVariation;
    private Double projectedEnergyReduction;
    private Double projectedEnergyReductionVariation;
    private Double projectedDemandReduction;
    private Double projectedDemandReductionVariation;
    private Double projectedEmissionsReduction;
    private Double projectedEmissionsReductionVariation;
    private Integer projectedPaybackYears;
    private Double projectedPaybackYearsVariation;
    private String riskRating;

    private Double costReductionRatio;
    private Double energyReductionIntensity;
    private Double costReductionIntensity;
    private Double emissionsReductionIntensity;

    private List<RetrofitOpportunityMeasureInput> inputs;

    public RetrofitOpportunityMeasure(){}

    public String getId() {
        return id;
    }

    public RetrofitOpportunityMeasure setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public RetrofitOpportunityMeasure setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public RetrofitOpportunityMeasure setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
        return this;
    }

    public String getAssetId() {
        return assetId;
    }

    public RetrofitOpportunityMeasure setAssetId(String assetId) {
        this.assetId = assetId;
        return this;
    }

    public String getSystemId() {
        return systemId;
    }

    public RetrofitOpportunityMeasure setSystemId(String systemId) {
        this.systemId = systemId;
        return this;
    }

    public String getComponentId() {
        return componentId;
    }

    public RetrofitOpportunityMeasure setComponentId(String componentId) {
        this.componentId = componentId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public RetrofitOpportunityMeasure setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getExpectedPaybackYears() {
        return expectedPaybackYears;
    }

    public RetrofitOpportunityMeasure setExpectedPaybackYears(Integer expectedPaybackYears) {
        this.expectedPaybackYears = expectedPaybackYears;
        return this;
    }

    public Integer getUsefulLifeYears() {
        return usefulLifeYears;
    }

    public RetrofitOpportunityMeasure setUsefulLifeYears(Integer usefulLifeYears) {
        this.usefulLifeYears = usefulLifeYears;
        return this;
    }

    public Double getReoccurringCost() {
        return reoccurringCost;
    }

    public RetrofitOpportunityMeasure setReoccurringCost(Double reoccurringCost) {
        this.reoccurringCost = reoccurringCost;
        return this;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public RetrofitOpportunityMeasure setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
        return this;
    }

    public Double getIncentive() {
        return incentive;
    }

    public RetrofitOpportunityMeasure setIncentive(Double incentive) {
        this.incentive = incentive;
        return this;
    }

    public Double getIntRateOfReturn() {
        return intRateOfReturn;
    }

    public RetrofitOpportunityMeasure setIntRateOfReturn(Double intRateOfReturn) {
        this.intRateOfReturn = intRateOfReturn;
        return this;
    }

    public Double getNetPresentValue() {
        return netPresentValue;
    }

    public RetrofitOpportunityMeasure setNetPresentValue(Double netPresentValue) {
        this.netPresentValue = netPresentValue;
        return this;
    }

    public Double getReturnOnInvestment() {
        return returnOnInvestment;
    }

    public RetrofitOpportunityMeasure setReturnOnInvestment(Double returnOnInvestment) {
        this.returnOnInvestment = returnOnInvestment;
        return this;
    }

    public Double getValueAtRisk() {
        return valueAtRisk;
    }

    public RetrofitOpportunityMeasure setValueAtRisk(Double valueAtRisk) {
        this.valueAtRisk = valueAtRisk;
        return this;
    }

    public Double getPerformanceRisk() {
        return performanceRisk;
    }

    public RetrofitOpportunityMeasure setPerformanceRisk(Double performanceRisk) {
        this.performanceRisk = performanceRisk;
        return this;
    }

    public String getName() {
        return name;
    }

    public RetrofitOpportunityMeasure setName(String name) {
        this.name = name;
        return this;
    }

    public Double getInitialCost() {
        return initialCost;
    }

    public RetrofitOpportunityMeasure setInitialCost(Double initialCost) {
        this.initialCost = initialCost;
        return this;
    }

    public Double getGrossCost() {
        return grossCost;
    }

    public RetrofitOpportunityMeasure setGrossCost(Double grossCost) {
        this.grossCost = grossCost;
        return this;
    }

    public Double getNetCost() {
        return netCost;
    }

    public RetrofitOpportunityMeasure setNetCost(Double netCost) {
        this.netCost = netCost;
        return this;
    }

    public Double getExpectedCostReduction() {
        return expectedCostReduction;
    }

    public RetrofitOpportunityMeasure setExpectedCostReduction(Double expectedCostReduction) {
        this.expectedCostReduction = expectedCostReduction;
        return this;
    }

    public Double getExpectedEnergyReduction() {
        return expectedEnergyReduction;
    }

    public RetrofitOpportunityMeasure setExpectedEnergyReduction(Double expectedEnergyReduction) {
        this.expectedEnergyReduction = expectedEnergyReduction;
        return this;
    }

    public Double getExpectedDemandReduction() {
        return expectedDemandReduction;
    }

    public RetrofitOpportunityMeasure setExpectedDemandReduction(Double expectedDemandReduction) {
        this.expectedDemandReduction = expectedDemandReduction;
        return this;
    }

    public Double getExpectedEmissionsReduction() {
        return expectedEmissionsReduction;
    }

    public RetrofitOpportunityMeasure setExpectedEmissionsReduction(Double expectedEmissionsReduction) {
        this.expectedEmissionsReduction = expectedEmissionsReduction;
        return this;
    }

    public Double getProjectedCostReduction() {
        return projectedCostReduction;
    }

    public RetrofitOpportunityMeasure setProjectedCostReduction(Double projectedCostReduction) {
        this.projectedCostReduction = projectedCostReduction;
        return this;
    }

    public Double getProjectedCostReductionVariation() {
        return projectedCostReductionVariation;
    }

    public RetrofitOpportunityMeasure setProjectedCostReductionVariation(Double projectedCostReductionVariation) {
        this.projectedCostReductionVariation = projectedCostReductionVariation;
        return this;
    }

    public Double getProjectedEnergyReduction() {
        return projectedEnergyReduction;
    }

    public RetrofitOpportunityMeasure setProjectedEnergyReduction(Double projectedEnergyReduction) {
        this.projectedEnergyReduction = projectedEnergyReduction;
        return this;
    }

    public Double getProjectedEnergyReductionVariation() {
        return projectedEnergyReductionVariation;
    }

    public RetrofitOpportunityMeasure setProjectedEnergyReductionVariation(Double projectedEnergyReductionVariation) {
        this.projectedEnergyReductionVariation = projectedEnergyReductionVariation;
        return this;
    }

    public Double getProjectedDemandReduction() {
        return projectedDemandReduction;
    }

    public RetrofitOpportunityMeasure setProjectedDemandReduction(Double projectedDemandReduction) {
        this.projectedDemandReduction = projectedDemandReduction;
        return this;
    }

    public Double getProjectedDemandReductionVariation() {
        return projectedDemandReductionVariation;
    }

    public RetrofitOpportunityMeasure setProjectedDemandReductionVariation(Double projectedDemandReductionVariation) {
        this.projectedDemandReductionVariation = projectedDemandReductionVariation;
        return this;
    }

    public Double getProjectedEmissionsReduction() {
        return projectedEmissionsReduction;
    }

    public RetrofitOpportunityMeasure setProjectedEmissionsReduction(Double projectedEmissionsReduction) {
        this.projectedEmissionsReduction = projectedEmissionsReduction;
        return this;
    }

    public Double getProjectedEmissionsReductionVariation() {
        return projectedEmissionsReductionVariation;
    }

    public RetrofitOpportunityMeasure setProjectedEmissionsReductionVariation(Double projectedEmissionsReductionVariation) {
        this.projectedEmissionsReductionVariation = projectedEmissionsReductionVariation;
        return this;
    }

    public Integer getProjectedPaybackYears() {
        return projectedPaybackYears;
    }

    public RetrofitOpportunityMeasure setProjectedPaybackYears(Integer projectedPaybackYears) {
        this.projectedPaybackYears = projectedPaybackYears;
        return this;
    }

    public Double getProjectedPaybackYearsVariation() {
        return projectedPaybackYearsVariation;
    }

    public RetrofitOpportunityMeasure setProjectedPaybackYearsVariation(Double projectedPaybackYearsVariation) {
        this.projectedPaybackYearsVariation = projectedPaybackYearsVariation;
        return this;
    }

    public String getRiskRating() {
        return riskRating;
    }

    public RetrofitOpportunityMeasure setRiskRating(String riskRating) {
        this.riskRating = riskRating;
        return this;
    }

    public Double getCostReductionRatio() {
        return costReductionRatio;
    }

    public RetrofitOpportunityMeasure setCostReductionRatio(Double costReductionRatio) {
        this.costReductionRatio = costReductionRatio;
        return this;
    }

    public Double getEnergyReductionIntensity() {
        return energyReductionIntensity;
    }

    public RetrofitOpportunityMeasure setEnergyReductionIntensity(Double energyReductionIntensity) {
        this.energyReductionIntensity = energyReductionIntensity;
        return this;
    }

    public Double getCostReductionIntensity() {
        return costReductionIntensity;
    }

    public RetrofitOpportunityMeasure setCostReductionIntensity(Double costReductionIntensity) {
        this.costReductionIntensity = costReductionIntensity;
        return this;
    }

    public Double getEmissionsReductionIntensity() {
        return emissionsReductionIntensity;
    }

    public RetrofitOpportunityMeasure setEmissionsReductionIntensity(Double emissionsReductionIntensity) {
        this.emissionsReductionIntensity = emissionsReductionIntensity;
        return this;
    }

    public List<RetrofitOpportunityMeasureInput> getInputs() {
        return inputs;
    }

    public RetrofitOpportunityMeasure setInputs(List<RetrofitOpportunityMeasureInput> inputs) {
        this.inputs = inputs;
        return this;
    }
}
