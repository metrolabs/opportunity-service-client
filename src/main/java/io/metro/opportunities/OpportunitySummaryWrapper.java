/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.metro.opportunities.models.RetrofitOpportunitySummaryAnnual;
import io.metro.opportunities.models.RetrofitOpportunitySummaryMonthly;

import java.util.List;
import java.util.Map;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OpportunitySummaryWrapper {
    private Integer usefulLifeYears;
    private Double reoccurringCost;
    private Double discountRate;
    private Double intRateOfReturn;
    private Double netPresentValue;
    private Double returnOnInvestment;
    private Double valueAtRisk;
    private Double performanceRisk;
    private String simulationId;
    private String name;
    private Double initialCost;
    private Double grossCost;
    private Double netCost;
    private Double incentive;
    private Double expectedCostReduction;
    private Double expectedEnergyReduction;
    private Double expectedDemandReduction;
    private Double expectedEmissionsReduction;
    private Integer expectedPaybackYears;
    private Double electricPriceKwh;
    private Double electricCo2Kwh;
    private Double fossilFuelPriceKwh;
    private Double fossilFuelCo2Kwh;
    private Double projectedCostReduction;
    private Double projectedCostReductionVariation;
    private Double projectedEnergyReduction;
    private Double projectedEnergyReductionVariation;
    private Double projectedDemandReduction;
    private Double projectedDemandReductionVariation;
    private Double projectedEmissionsReduction;
    private Double projectedEmissionsReductionVariation;
    private Integer projectedPaybackYears;
    private Double projectedPaybackYearsVariation;
    private Double totalIncentive;
    private String riskRating;
    private Map<String, RetrofitOpportunitySummaryAnnual> annualSummary;
    private Map<String, List<RetrofitOpportunitySummaryMonthly>> monthlySummary;
    private Set<String> buildings;
    private Set<String> opportunities;

    private Double costReductionRatio;
    private Double energyReductionIntensity;
    private Double costReductionIntensity;
    private Double emissionsReductionIntensity;

    public OpportunitySummaryWrapper() {
    }

    public Integer getUsefulLifeYears() {
        return usefulLifeYears;
    }

    public OpportunitySummaryWrapper setUsefulLifeYears(Integer usefulLifeYears) {
        this.usefulLifeYears = usefulLifeYears;
        return this;
    }

    public Double getReoccurringCost() {
        return reoccurringCost;
    }

    public OpportunitySummaryWrapper setReoccurringCost(Double reoccurringCost) {
        this.reoccurringCost = reoccurringCost;
        return this;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public OpportunitySummaryWrapper setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
        return this;
    }

    public Double getIntRateOfReturn() {
        return intRateOfReturn;
    }

    public OpportunitySummaryWrapper setIntRateOfReturn(Double intRateOfReturn) {
        this.intRateOfReturn = intRateOfReturn;
        return this;
    }

    public Double getNetPresentValue() {
        return netPresentValue;
    }

    public OpportunitySummaryWrapper setNetPresentValue(Double netPresentValue) {
        this.netPresentValue = netPresentValue;
        return this;
    }

    public Double getReturnOnInvestment() {
        return returnOnInvestment;
    }

    public OpportunitySummaryWrapper setReturnOnInvestment(Double returnOnInvestment) {
        this.returnOnInvestment = returnOnInvestment;
        return this;
    }

    public Double getValueAtRisk() {
        return valueAtRisk;
    }

    public OpportunitySummaryWrapper setValueAtRisk(Double valueAtRisk) {
        this.valueAtRisk = valueAtRisk;
        return this;
    }

    public Double getPerformanceRisk() {
        return performanceRisk;
    }

    public OpportunitySummaryWrapper setPerformanceRisk(Double performanceRisk) {
        this.performanceRisk = performanceRisk;
        return this;
    }

    public String getSimulationId() {
        return simulationId;
    }

    public OpportunitySummaryWrapper setSimulationId(String simulationId) {
        this.simulationId = simulationId;
        return this;
    }

    public String getName() {
        return name;
    }

    public OpportunitySummaryWrapper setName(String name) {
        this.name = name;
        return this;
    }

    public Double getInitialCost() {
        return initialCost;
    }

    public OpportunitySummaryWrapper setInitialCost(Double initialCost) {
        this.initialCost = initialCost;
        return this;
    }

    public Double getGrossCost() {
        return grossCost;
    }

    public OpportunitySummaryWrapper setGrossCost(Double grossCost) {
        this.grossCost = grossCost;
        return this;
    }

    public Double getNetCost() {
        return netCost;
    }

    public OpportunitySummaryWrapper setNetCost(Double netCost) {
        this.netCost = netCost;
        return this;
    }

    public Double getIncentive() {
        return incentive;
    }

    public OpportunitySummaryWrapper setIncentive(Double incentive) {
        this.incentive = incentive;
        return this;
    }

    public Double getExpectedCostReduction() {
        return expectedCostReduction;
    }

    public OpportunitySummaryWrapper setExpectedCostReduction(Double expectedCostReduction) {
        this.expectedCostReduction = expectedCostReduction;
        return this;
    }

    public Double getExpectedEnergyReduction() {
        return expectedEnergyReduction;
    }

    public OpportunitySummaryWrapper setExpectedEnergyReduction(Double expectedEnergyReduction) {
        this.expectedEnergyReduction = expectedEnergyReduction;
        return this;
    }

    public Double getExpectedDemandReduction() {
        return expectedDemandReduction;
    }

    public OpportunitySummaryWrapper setExpectedDemandReduction(Double expectedDemandReduction) {
        this.expectedDemandReduction = expectedDemandReduction;
        return this;
    }

    public Double getExpectedEmissionsReduction() {
        return expectedEmissionsReduction;
    }

    public OpportunitySummaryWrapper setExpectedEmissionsReduction(Double expectedEmissionsReduction) {
        this.expectedEmissionsReduction = expectedEmissionsReduction;
        return this;
    }

    public Integer getExpectedPaybackYears() {
        return expectedPaybackYears;
    }

    public OpportunitySummaryWrapper setExpectedPaybackYears(Integer expectedPaybackYears) {
        this.expectedPaybackYears = expectedPaybackYears;
        return this;
    }

    public Double getElectricPriceKwh() {
        return electricPriceKwh;
    }

    public OpportunitySummaryWrapper setElectricPriceKwh(Double electricPriceKwh) {
        this.electricPriceKwh = electricPriceKwh;
        return this;
    }

    public Double getElectricCo2Kwh() {
        return electricCo2Kwh;
    }

    public OpportunitySummaryWrapper setElectricCo2Kwh(Double electricCo2Kwh) {
        this.electricCo2Kwh = electricCo2Kwh;
        return this;
    }

    public Double getFossilFuelPriceKwh() {
        return fossilFuelPriceKwh;
    }

    public OpportunitySummaryWrapper setFossilFuelPriceKwh(Double fossilFuelPriceKwh) {
        this.fossilFuelPriceKwh = fossilFuelPriceKwh;
        return this;
    }

    public Double getFossilFuelCo2Kwh() {
        return fossilFuelCo2Kwh;
    }

    public OpportunitySummaryWrapper setFossilFuelCo2Kwh(Double fossilFuelCo2Kwh) {
        this.fossilFuelCo2Kwh = fossilFuelCo2Kwh;
        return this;
    }

    public Double getProjectedCostReduction() {
        return projectedCostReduction;
    }

    public OpportunitySummaryWrapper setProjectedCostReduction(Double projectedCostReduction) {
        this.projectedCostReduction = projectedCostReduction;
        return this;
    }

    public Double getProjectedCostReductionVariation() {
        return projectedCostReductionVariation;
    }

    public OpportunitySummaryWrapper setProjectedCostReductionVariation(Double projectedCostReductionVariation) {
        this.projectedCostReductionVariation = projectedCostReductionVariation;
        return this;
    }

    public Double getProjectedEnergyReduction() {
        return projectedEnergyReduction;
    }

    public OpportunitySummaryWrapper setProjectedEnergyReduction(Double projectedEnergyReduction) {
        this.projectedEnergyReduction = projectedEnergyReduction;
        return this;
    }

    public Double getProjectedEnergyReductionVariation() {
        return projectedEnergyReductionVariation;
    }

    public OpportunitySummaryWrapper setProjectedEnergyReductionVariation(Double projectedEnergyReductionVariation) {
        this.projectedEnergyReductionVariation = projectedEnergyReductionVariation;
        return this;
    }

    public Double getProjectedDemandReduction() {
        return projectedDemandReduction;
    }

    public OpportunitySummaryWrapper setProjectedDemandReduction(Double projectedDemandReduction) {
        this.projectedDemandReduction = projectedDemandReduction;
        return this;
    }

    public Double getProjectedDemandReductionVariation() {
        return projectedDemandReductionVariation;
    }

    public OpportunitySummaryWrapper setProjectedDemandReductionVariation(Double projectedDemandReductionVariation) {
        this.projectedDemandReductionVariation = projectedDemandReductionVariation;
        return this;
    }

    public Double getProjectedEmissionsReduction() {
        return projectedEmissionsReduction;
    }

    public OpportunitySummaryWrapper setProjectedEmissionsReduction(Double projectedEmissionsReduction) {
        this.projectedEmissionsReduction = projectedEmissionsReduction;
        return this;
    }

    public Double getProjectedEmissionsReductionVariation() {
        return projectedEmissionsReductionVariation;
    }

    public OpportunitySummaryWrapper setProjectedEmissionsReductionVariation(Double projectedEmissionsReductionVariation) {
        this.projectedEmissionsReductionVariation = projectedEmissionsReductionVariation;
        return this;
    }

    public Integer getProjectedPaybackYears() {
        return projectedPaybackYears;
    }

    public OpportunitySummaryWrapper setProjectedPaybackYears(Integer projectedPaybackYears) {
        this.projectedPaybackYears = projectedPaybackYears;
        return this;
    }

    public Double getProjectedPaybackYearsVariation() {
        return projectedPaybackYearsVariation;
    }

    public OpportunitySummaryWrapper setProjectedPaybackYearsVariation(Double projectedPaybackYearsVariation) {
        this.projectedPaybackYearsVariation = projectedPaybackYearsVariation;
        return this;
    }

    public Double getTotalIncentive() {
        return totalIncentive;
    }

    public OpportunitySummaryWrapper setTotalIncentive(Double totalIncentive) {
        this.totalIncentive = totalIncentive;
        return this;
    }

    public String getRiskRating() {
        return riskRating;
    }

    public OpportunitySummaryWrapper setRiskRating(String riskRating) {
        this.riskRating = riskRating;
        return this;
    }

    public Double getCostReductionRatio() {
        return costReductionRatio;
    }

    public OpportunitySummaryWrapper setCostReductionRatio(Double costReductionRatio) {
        this.costReductionRatio = costReductionRatio;
        return this;
    }

    public Double getEnergyReductionIntensity() {
        return energyReductionIntensity;
    }

    public OpportunitySummaryWrapper setEnergyReductionIntensity(Double energyReductionIntensity) {
        this.energyReductionIntensity = energyReductionIntensity;
        return this;
    }

    public Double getCostReductionIntensity() {
        return costReductionIntensity;
    }

    public OpportunitySummaryWrapper setCostReductionIntensity(Double costReductionIntensity) {
        this.costReductionIntensity = costReductionIntensity;
        return this;
    }

    public Double getEmissionsReductionIntensity() {
        return emissionsReductionIntensity;
    }

    public OpportunitySummaryWrapper setEmissionsReductionIntensity(Double emissionsReductionIntensity) {
        this.emissionsReductionIntensity = emissionsReductionIntensity;
        return this;
    }

    public Map<String, RetrofitOpportunitySummaryAnnual> getAnnualSummary() {
        return annualSummary;
    }

    public OpportunitySummaryWrapper setAnnualSummary(Map<String, RetrofitOpportunitySummaryAnnual> data) {
        this.annualSummary = data;
        return this;
    }

    public Map<String, List<RetrofitOpportunitySummaryMonthly>> getMonthlySummary() {
        return monthlySummary;
    }

    public OpportunitySummaryWrapper setMonthlySummary(Map<String, List<RetrofitOpportunitySummaryMonthly>> data) {
        this.monthlySummary = data;
        return this;
    }

    public Set<String> getBuildings() {
        return buildings;
    }

    public OpportunitySummaryWrapper setBuildings(Set<String> buildings) {
        this.buildings = buildings;
        return this;
    }

    public Set<String> getOpportunities() {
        return opportunities;
    }

    public OpportunitySummaryWrapper setOpportunities(Set<String> opportunities) {
        this.opportunities = opportunities;
        return this;
    }
}
