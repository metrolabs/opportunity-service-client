/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.opportunities;

import io.metro.opportunities.models.RetrofitOpportunity;
import io.metro.opportunities.models.RetrofitOpportunityEconomics;
import io.metro.opportunities.models.RetrofitOpportunityMeasure;
import io.metro.opportunities.models.RetrofitOpportunityMeasureInput;
import io.metro.specification.Filter;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;

import static org.hibernate.validator.internal.util.Contracts.assertValueNotNull;

@Service
public class OpportunityClient {
    private final Logger logger = LoggerFactory.getLogger(OpportunityClient.class);

    @Value("${spring.api.gateway-service}")
    private String SERVICE;
    @Value("${spring.api.username}")
    private String username;
    @Value("${spring.api.password}")
    private String password;

    private static final String OPPORTUNITIES = "/opportunities";
    private static final String MEASURES = "/measures";
    private static final String INPUTS = "/inputs";

    private RestTemplate restTemplate;
    private HttpHeaders headers;

    @Autowired
    public OpportunityClient(@LoadBalanced RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
        headers = new HttpHeaders();
    }

    public OpportunityClient init() {
        System.out.println("OpportunityClient PostConstruct Initialized");
        logger.debug("");
        logger.debug("====================================================================================");
        logger.debug("========================== initializing MeasurableClient ===========================");
        logger.debug("Authenticating Client with username: {} and password: {}", username, password);
        authenticate(username, password);
        logger.debug("Setting header X-Forwarded-Host: {}", getXForwardedHost(SERVICE));
        headers.set("X-Forwarded-Host", getXForwardedHost(SERVICE));
        logger.debug("===========================   initialization completed   ===========================");
        logger.debug("");

        return this;
    }

    private String getXForwardedHost(String url) {
        if(url.toLowerCase().contains("https://")) {
            return url.toLowerCase().replaceFirst("https://", "");
        }
        else if (url.toLowerCase().contains("http://")) {
            return url.toLowerCase().replaceFirst("http://", "");
        }

        return url;
    }

    public OpportunityClient authenticate(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String( encodedAuth );
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, authHeader);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        HttpEntity<Object> response = restTemplate.exchange(SERVICE, HttpMethod.GET, httpEntity, Object.class);

        String sessionToken = response.getHeaders().getFirst("x-auth-token");
        assertValueNotNull(sessionToken, "Response: x-auth-token");

        withAuthToken(sessionToken);
        logger.debug("Setting header X-Auth-Token: {}", sessionToken);

        return this;
    }

    /**
     * Override the URL of the service to use.
     * Note: URL is stripped of trailing slash.
     *
     * @param url URL of service
     * @return this client
     */
    public OpportunityClient withUrl(String url) {
        SERVICE = url.substring(0, url.length() - (url.endsWith("/") ? 1 : 0));
        return this;
    }

    public OpportunityClient withAuthToken(String auth) {
        headers.set("x-auth-token", auth);
        return this;
    }


    /**
     * Request the persistence of a new project
     *
     * @param entity {@link RetrofitOpportunity}
     * @return {@link Resources<RetrofitOpportunity>}
     */
    public ResponseEntity<Resource<RetrofitOpportunity>> createOpportunity(RetrofitOpportunity entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .build();

        return restTemplate.exchange(builder.toString(), HttpMethod.POST,
                new HttpEntity<>(entity, headers), new ParameterizedTypeReference<Resource<RetrofitOpportunity>>() {});
    }

    /**
     * Request the persistence of a new project
     *
     * @param id the {@link RetrofitOpportunity} id
     * @param entity {@link RetrofitOpportunity}
     * @return {@link Resources<RetrofitOpportunity>}
     */
    public ResponseEntity<Resource<RetrofitOpportunity>> updateOpportunity(String id, RetrofitOpportunity entity)
    {

        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.PUT,
                new HttpEntity<>(entity, headers), new ParameterizedTypeReference<Resource<RetrofitOpportunity>>() {});
    }

    /**
     * Request the persistence of a new project
     *
     * @param id the {@link RetrofitOpportunity} id
     * @param entity {@link RetrofitOpportunity}
     * @return {@link Resources<RetrofitOpportunity>}
     */
    public ResponseEntity<Resource<RetrofitOpportunity>> patchOpportunity(String id, RetrofitOpportunity entity)
    {

        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.PATCH,
                new HttpEntity<>(entity, headers), new ParameterizedTypeReference<Resource<RetrofitOpportunity>>() {});
    }

    /**
     * Send delete request for a project {@link RetrofitOpportunity}
     *
     * @param id the RetrofitOpportunity id
     */
    public ResponseEntity deleteOpportunity(String id)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.DELETE, new HttpEntity<>(headers), Object.class);
    }

    /**
     * Request a project
     *
     * @param id the RetrofitOpportunity id
     * @return {@link Resource<RetrofitOpportunity>}
     */
    public ResponseEntity<Resource<RetrofitOpportunity>> getOpportunity(String id)
    {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resource<RetrofitOpportunity>>() { });
    }

    /**
     * Request RetrofitOpportunity's Economics Report
     *
     * @param id the RetrofitOpportunity id
     * @return {@link Resources<RetrofitOpportunityEconomics>}
     */
    public ResponseEntity<RetrofitOpportunityEconomics> getOpportunityEconomicsReport(String id)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}/economics-report")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<RetrofitOpportunityEconomics> () {});
    }

    /**
     * Return paginated collection of RetrofitOpportunity's
     *
     * @param page pagination offset
     * @param size number of records to return
     * @return {@link PagedResources<Resource<RetrofitOpportunity>>}
     */
    public ResponseEntity<PagedResources<Resource<RetrofitOpportunity>>> findOpportunities(int page, int size)
    {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .queryParam("page", page)
                .queryParam("size", size)
                .build();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<RetrofitOpportunity>>>() { });
    }

    /**
     * Return paginated collection of RetrofitOpportunity's constrained by filter search criteria
     *
     * @param filter the filter search criteria
     * @param page pagination offset
     * @param size number of records to return
     * @return {@link PagedResources<Resource<RetrofitOpportunity>>}
     */
    public ResponseEntity<PagedResources<Resource<RetrofitOpportunity>>>
    searchForOpportunity(Filter filter, int page, int size)
    {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/search")
                .queryParam("page", page)
                .queryParam("size", size)
                .build();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(filter, headers),
                new ParameterizedTypeReference<PagedResources<Resource<RetrofitOpportunity>>>() { });
    }

    /**
     * Return paginated collection of RetrofitOpportunity's constrained by filter search criteria
     *
     * @param filter the filter search criteria
     * @return {@link MetroResource < OpportunitySummaryWrapper >}
     */
    public ResponseEntity<MetroResource<OpportunitySummaryWrapper>>
    getOpportunityAggregation(Filter filter)
    {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/aggregation")
                .build();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(filter, headers),
                new ParameterizedTypeReference<MetroResource<OpportunitySummaryWrapper>>() { });
    }

    /**
     * Request the persistence of a new opportunity measure
     *
     * @param id the opportunity id
     * @param entity {@link RetrofitOpportunityMeasure}
     * @return {@link Resources<RetrofitOpportunityMeasure>}
     */
    public ResponseEntity createOpportunityMeasure(String id, RetrofitOpportunityMeasure entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}/measures")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.POST, new HttpEntity<>(entity, headers),
                new ParameterizedTypeReference<Resources<RetrofitOpportunityMeasure>>() { });
    }

    /**
     * Request a collection of opportunity measures
     *
     * @param id opportunity id
     * @param page pagination offset
     * @param size number of records to return
     * @return {@link Resources<RetrofitOpportunityMeasure>}
     */
    public ResponseEntity<Resources<RetrofitOpportunityMeasure>>
    getOpportunityMeasure(String id, int page, int size)
    {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(SERVICE)
                .path(OPPORTUNITIES)
                .path("/{id}/measures")
                .queryParam("page", page)
                .queryParam("size", size)
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resources<RetrofitOpportunityMeasure>>() { });
    }



    /*
     * Opportunity Measure endpoint methods
     */

    /**
     * Persist updated {@link RetrofitOpportunityMeasure}
     * 
     * @param id opportunity measure id
     * @param entity {@link RetrofitOpportunityMeasure}
     * @return {@link Resources<RetrofitOpportunityMeasure>}
     */
    public ResponseEntity<Resource<RetrofitOpportunityMeasure>> updateOpportunityMeasure(
            String id, 
            RetrofitOpportunityMeasure entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        HttpEntity<RetrofitOpportunityMeasure> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<RetrofitOpportunityMeasure>> PTR =
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasure>>() {};
        return restTemplate.exchange(builder.toUri(), HttpMethod.PUT, REQUEST, PTR);
    }

    /**
     * Persist partial updates to {@link RetrofitOpportunityMeasure}
     *
     * @param id opportunity measure id
     * @param entity {@link RetrofitOpportunityMeasure}
     * @return {@link Resources<RetrofitOpportunityMeasure>}
     */
    public ResponseEntity<Resource<RetrofitOpportunityMeasure>> patchOpportunityMeasure(
            String id,
            RetrofitOpportunityMeasure entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        HttpEntity<RetrofitOpportunityMeasure> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<RetrofitOpportunityMeasure>> PTR =
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasure>>() {};
        return restTemplate.exchange(builder.toUri(), HttpMethod.PATCH, REQUEST, PTR);
    }

    /**
     * Delete a {@link RetrofitOpportunityMeasure}
     * 
     * @param id opportunity measure id
     * @return {@link ResponseEntity}
     */
    public ResponseEntity deleteOpportunityMeasure(String id)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        HttpEntity<RetrofitOpportunityMeasure> REQUEST = new HttpEntity<>(headers);
        return restTemplate.exchange(builder.toUri(), HttpMethod.DELETE, REQUEST, Object.class);
    }

    /**
     * 
     * @param id opportunity measure id
     * @return {@link Resources<RetrofitOpportunityMeasure>}
     */
    public ResponseEntity<Resource<RetrofitOpportunityMeasure>> getOpportunityMeasure(String id)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        HttpEntity<RetrofitOpportunityMeasure> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resource<RetrofitOpportunityMeasure>> PTR =
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasure>>() {};
        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, REQUEST, PTR);
    }

    /**
     * Persist a new {@link RetrofitOpportunityMeasure}
     * 
     * @param id opportunity measure id
     * @param entity {@link RetrofitOpportunityMeasure}
     * @return {@link Resources<RetrofitOpportunityMeasure>}
     */
    public ResponseEntity<Resource<RetrofitOpportunityMeasureInput>> createOpportunityMeasureInput(
            String id, 
            RetrofitOpportunityMeasureInput entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURES)
                .path("/{id}")
                .path(INPUTS)
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.POST, new HttpEntity<>(entity, headers),
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasureInput>>() { });
    }

    /**
     * Return paginated collection of {@link RetrofitOpportunityMeasureInput}
     * 
     * @param id opportunity measure id
     * @param page pagination offset
     * @param size number of records to return
     * @return {@link PagedResources<Resource<RetrofitOpportunityMeasureInput>>}
     */
    public ResponseEntity<PagedResources<Resource<RetrofitOpportunityMeasureInput>>> getMeasureInputCollection(
            String id, 
            int page, 
            int size)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(MEASURES)
                .path("/{id}")
                .path(INPUTS)
                .queryParam("page", page)
                .queryParam("size", size)
                .buildAndExpand(id);

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<RetrofitOpportunityMeasureInput>>>() { });
    }
    
    
    
    /*
     * Opportunity Measure Input endpoint methods
     */
    public ResponseEntity<Resource<RetrofitOpportunityMeasureInput>> updateMeasureInput(
            String id,
            RetrofitOpportunityMeasureInput entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(INPUTS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.PUT, new HttpEntity<>(entity, headers),
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasureInput>>() {});
    }

    public ResponseEntity<Resource<RetrofitOpportunityMeasureInput>> patchMeasureInput(
            String id,
            RetrofitOpportunityMeasureInput entity)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(INPUTS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.PATCH, new HttpEntity<>(entity, headers),
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasureInput>>() {});
    }

    public ResponseEntity deleteMeasureInput(String id)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(INPUTS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        HttpEntity<RetrofitOpportunityMeasureInput> REQUEST = new HttpEntity<>(headers);
        return restTemplate.exchange(builder.toUri(), HttpMethod.DELETE, REQUEST, Object.class);
    }

    public ResponseEntity<Resource<RetrofitOpportunityMeasureInput>> getMeasureInput(String id)
    {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(INPUTS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resource<RetrofitOpportunityMeasureInput>>() {});
    }
}
