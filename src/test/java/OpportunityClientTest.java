/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

import io.metro.opportunities.OpportunityClient;
import io.metro.opportunities.models.RetrofitOpportunity;
import io.metro.opportunities.models.RetrofitOpportunityEconomics;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestConfig.class)
@SpringBootTest(classes={OpportunityClient.class, RestTemplateFilter.class})
public class OpportunityClientTest {

    private static final Logger logger = LoggerFactory.getLogger(OpportunityClientTest.class);

    @Autowired
    private OpportunityClient client;

    private String buildingId = "BUILDING1471994530429QNEjxZ";
    private String opportubityId = "RO0ee89c6443272d866f2ef964322471b6c1b233ce";


    @Before
    public void before() {
        client.init();
    }

    @Test
    public void getOpportunity() {
        ResponseEntity<Resource<RetrofitOpportunity>> response = client.getOpportunity(opportubityId);
        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertTrue(response.getBody().getId().getHref().endsWith(opportubityId));
    }


    @Test
    public void getOpportunityEconomicsReport() {
        ResponseEntity<RetrofitOpportunityEconomics> response = client.getOpportunityEconomicsReport(opportubityId);
        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void findOpportunities() {
        ResponseEntity<PagedResources<Resource<RetrofitOpportunity>>> response =
                client.findOpportunities(0, 1);
        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
    }
}
